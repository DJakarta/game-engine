package eu.djakarta;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;

public class CanvasDemo {

  private Frame mainFrame;

  public CanvasDemo() {
    prepareGUI();
  }

  public static void main(String[] args) {
    CanvasDemo awtControlDemo = new CanvasDemo();
    awtControlDemo.showCanvasDemo();
  }

  private void prepareGUI() {
    mainFrame = new Frame("Java AWT Examples");
    mainFrame.setSize(400, 400);
    mainFrame.setLocation(480, 200);
    mainFrame.setBackground(new Color(1f, 1f, 0f));
    mainFrame.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent windowEvent) {
        System.exit(0);
      }
    });

    mainFrame.setVisible(true);
  }

  private void showCanvasDemo() {
    mainFrame.add(new MyCanvas());
    mainFrame.setVisible(true);
  }

  class MyCanvas extends Canvas {

    public MyCanvas() {
      setBackground(new Color(0, 0, 1, (float) 0.1));
      setSize(300, 300);
    }

    public void paint(Graphics g) {
      Graphics2D g2;
      g2 = (Graphics2D) g;

      g2.setStroke(
          new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, null, 0.0f));
      g2.draw(new Ellipse2D.Double(0, 0, 100, 100));
    }
  }
}